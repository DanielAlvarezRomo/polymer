#Imagen base
FROM node:latest

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD build/default /apzp/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comandos de ejecición de la aplicación
CMD ["npm", "start"]
